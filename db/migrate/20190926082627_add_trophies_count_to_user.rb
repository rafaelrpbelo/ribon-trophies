class AddTrophiesCountToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :trophies_count, :integer, null: false, default: 0
  end
end
