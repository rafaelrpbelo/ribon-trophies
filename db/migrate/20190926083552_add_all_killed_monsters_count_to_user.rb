class AddAllKilledMonstersCountToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :all_killed_monsters_count, :integer, null: false, default: 0
  end
end
