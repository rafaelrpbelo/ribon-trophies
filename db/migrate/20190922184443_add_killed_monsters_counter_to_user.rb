class AddKilledMonstersCounterToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :killed_monsters_count, :json, default: {}
  end
end
