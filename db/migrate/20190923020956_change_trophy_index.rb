class ChangeTrophyIndex < ActiveRecord::Migration[6.0]
  def change
    remove_index :trophies, [:user_id, :type, :level]
    add_index :trophies, [:user_id, :type, :level]
  end

  def down
    remove_index :trophies, [:user_id, :type, :level]
    add_index :trophies, [:user_id, :type, :level], unique: true
  end
end
