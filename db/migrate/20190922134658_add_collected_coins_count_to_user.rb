class AddCollectedCoinsCountToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :collected_coins_count, :bigint, default: 0, null: false
  end
end
