class CreateTrophy < ActiveRecord::Migration[6.0]
  def change
    create_table :trophies do |t|
      t.references :user, null: false, foreign_key: true
      t.string :name, null: false
      t.string :type, null: false
      t.integer :level

      t.index [:user_id, :type, :level], unique: true
      t.timestamps
    end
  end
end
