class AddDeathsCountToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :deaths_count, :bigint, null: false, default: 0
  end
end
