require 'rails_helper'

describe KilledMonsterTrophy do
  let(:user) { create(:user) }
  let(:turtle) { create(:monster, name: 'Turtle') }
  let(:bowser) { create(:monster, name: 'Bowser') }

  context '.check_award_for' do
    context 'creates trophies to user' do
      it 'when user has same amount of coin than the target' do
        user.update!(killed_monsters_count: {
          turtle.id => 1
        })

        expect{ described_class.check_award_for(user) }.
          to change{ user.trophies.count }.by(1)
      end

      it 'when user has more amount of coin than the target' do
        user.update!(killed_monsters_count: {
          turtle.id => 2
        })

        expect{ described_class.check_award_for(user) }.
          to change{ user.trophies.count }.by(1)
      end

      it 'for each level if user has coins enough' do
        user.update!(killed_monsters_count: {
          turtle.id => 1_000_000_000
        })

        expect{ described_class.check_award_for(user) }.
          to change{ user.trophies.count }.from(0).to(5)
      end
    end

    it 'creates a trophy for each monster' do
      user.update!(killed_monsters_count: {
        turtle.id => 1,
        bowser.id => 1
      })

      expect{ described_class.check_award_for(user) }.
        to change{ user.trophies.count }.by(2)
    end
  end
end
