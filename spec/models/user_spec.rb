require 'rails_helper'

describe User do
  let(:user) { create(:user) }
  let(:turtle) { create(:monster, name: 'Turtle') }
  let(:bowser) { create(:monster, name: 'Bowser') }

  let(:user_gets_a_coin) do
    -> { user.collected_coins.create!(value: 10) }
  end

  let(:user_dies) do
    -> { user.deaths.create!(timestamp: 1.second.ago) }
  end

  let(:user_kills_a_turtle) do
    -> { user.killed_monsters.create!(monster: turtle) }
  end

  let(:user_kills_a_bowser) do
    -> { user.killed_monsters.create!(monster: bowser) }
  end

  context 'counters' do
    it 'increments collected_coins_count properly' do
      expect{ user_gets_a_coin.call }.to change{ user.collected_coins_count }.by(1)
    end

    it 'increments deaths_count properly' do
      expect{ user_dies.call }.to change{ user.deaths_count }.by(1)
    end

    it 'increments all_killed_monsters_count properly' do
      expect{
        user_kills_a_turtle.call
        user_kills_a_bowser.call
      }.to change(user, :all_killed_monsters_count).by(2)
    end

    it 'increments killed_monsters_count properly' do
      expect{ user_kills_a_turtle.call }.to change{
        user.killed_monsters_count.fetch(turtle.id.to_s, 0)
      }.by(1)
    end

    it 'decrements killed_monsters_count properly' do
      3.times { user_kills_a_turtle.call }

      expect{ user.killed_monsters.find_by(monster: turtle).destroy }.to change{
        user.killed_monsters_count.fetch(turtle.id.to_s, 0)
      }.from(3).to(2)
    end
  end

  context 'earns trophy' do
    it 'increases trophies_count' do
      expect{ user_gets_a_coin.call }.to change(user, :trophies_count).by(1)
    end

    context 'collected coin' do
      before do
        expect(user.collected_coins_count).to eq 0
      end

      CollectedCoinTrophy::LEVELS.each.with_index(1) do |level, index|
        it "level #{level[:number]}" do
          user.update! collected_coins_count: level[:target] - 1
          expect{ user_gets_a_coin.call }.to change{ user.trophies.count }.by(index)
        end
      end
    end

    context 'death' do
      before do
        expect(user.deaths_count).to eq 0
      end

      DeathTrophy::LEVELS.each.with_index(1) do |level, index|
        it "level #{level[:number]}" do
          user.update! deaths_count: level[:target] - 1
          expect{ user_dies.call }.to change{ user.trophies.count }.by(index)
        end
      end
    end

    context 'killed monster' do
      context 'turtle' do
        KilledMonsterTrophy::LEVELS.each.with_index(1) do |level, index|
          it "level #{level[:number]}" do
            user.killed_monsters_count[turtle.id.to_s] = level[:target] - 1

            expect{ user_kills_a_turtle.call }.
              to change{ user.trophies.count }.by(index)
          end
        end
      end

      context 'bowser' do
        KilledMonsterTrophy::LEVELS.each.with_index(1) do |level, index|
          it "level #{level[:number]}" do
            user.killed_monsters_count[bowser.id.to_s] = level[:target] - 1

            expect{ user_kills_a_bowser.call }.
              to change{ user.trophies.count }.by(index)
          end
        end
      end
    end
  end
end
