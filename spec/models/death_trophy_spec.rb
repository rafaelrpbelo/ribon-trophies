require 'rails_helper'

describe DeathTrophy do
  let(:user) { create(:user) }

  context '.check_award_for' do
    context 'creates trophies to user' do
      it 'when user has same amount of death than the target' do
        user.update!(deaths_count: 1)

        expect{ described_class.check_award_for(user) }.
          to change{ user.trophies.count }.by(1)
      end

      it 'when user has more amount of death than the target' do
        user.update!(deaths_count: 2)

        expect{ described_class.check_award_for(user) }.
          to change{ user.trophies.count }.by(1)
      end

      it 'for each level if user has deaths enough' do
        user.update!(deaths_count: 1_000_000_000)

        expect{ described_class.check_award_for(user) }.
          to change{ user.trophies.count }.from(0).to(5)
      end
    end
  end
end
