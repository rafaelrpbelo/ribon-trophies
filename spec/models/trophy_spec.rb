require 'rails_helper'

describe Trophy do
  let(:user) { create(:user) }

  context '.check_awards_for' do
    {
      CollectedCoin => CollectedCoinTrophy,
      Death         => DeathTrophy,
      KilledMonster => KilledMonsterTrophy
    }.
    each do |award_type, trophy_checker|
      it "should use #{trophy_checker} for #{award_type} award" do
        expect(trophy_checker).to receive(:check_award_for).with(user)
        Trophy.check_awards_for(user, type: award_type)
      end
    end
  end
end
