require 'rails_helper'

describe Api::UsersController do
  render_views

  let(:create_a_valid_user) do
    post :create, params: { user: attributes_for(:user) }
  end

  let(:create_a_invalid_user) do
    post :create, params: { user: {name: ''} }
  end

  before do
    request.env['HTTP_ACCEPT'] = 'application/json'
  end

  describe 'GET index' do
    it 'renders all users as json properly' do
      first_user, second_user = create_list(:user, 2)

      get :index

      expect(response.parsed_body).to eq({
        'results' => [
          { 'id' => first_user.id, 'name' => first_user.name },
          { 'id' => second_user.id, 'name' => second_user.name }
        ]
      })
    end
  end

  describe 'GET show' do
    let(:monster) { create(:monster, name: 'Turtle') }

    let(:user) do
      create(:user,
        name: 'Jhon Doe',
        collected_coins_count: 15,
        deaths_count: 3,
        all_killed_monsters_count: 32,
        killed_monsters_count: { monster.id => 7 },
        created_at: Time.new(2019,1,1),
        updated_at: Time.new(2019,1,2))
    end

    let!(:trophy) do
      create(:trophy,
        name: 'My First Trophy',
        user: user,
        level: 1)
    end

    it 'renders user attributes properly' do
      get :show, params: { id: user.id }

      expect(response.parsed_body).to eq({
        'result' => {
          'id' => user.id,
          'name' => 'Jhon Doe',
          'collected_coins_count' => 15,
          'deaths_count' => 3,
          'trophies_count' => 1,
          'all_killed_monsters_count' => 32,
          'killed_monsters_count' => {
            monster.id.to_s => 7
          },
          'trophies' => [
            {
              'id' => trophy.id,
              'name' => trophy.name,
              'level' => trophy.level
            }
          ],
          'created_at' => Time.new(2019,1,1).as_json,
          'updated_at' => Time.new(2019,1,2).as_json
        }
      })
    end
  end

  describe 'POST create' do
    context 'with valid params' do
      it 'creates a new user' do
        expect{ create_a_valid_user }.to change(User, :count).by(1)
      end

      it 'returns status as created' do
        create_a_valid_user
        expect(response.status).to eq 201
      end
    end

    context 'with invalid params' do
      it 'does not create a new user' do
        expect{ create_a_invalid_user }.to_not change(User, :count)
      end

      it 'returns status as ok' do
        create_a_invalid_user
        expect(response.status).to eq 200
      end

      it 'returns errors' do
        create_a_invalid_user
        expect(response.parsed_body).to eq({
          "errors" => ["Name can't be blank"]
        })
      end
    end
  end
end
