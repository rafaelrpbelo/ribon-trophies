require 'rails_helper'

describe Api::MonstersController do
  render_views

  let(:create_a_valid_monster) do
    post :create, params: { monster: attributes_for(:monster) }
  end

  let(:create_a_invalid_monster) do
    post :create, params: { monster: {name: ''} }
  end

  before do
    request.env['HTTP_ACCEPT'] = 'application/json'
  end

  describe 'GET index' do
    it 'renders all monsters as json properly' do
      first_monster, second_monster = create_list(:monster, 2)

      get :index

      expect(response.parsed_body).to eq({
        'results' => [
          { 'id' => first_monster.id, 'name' => first_monster.name },
          { 'id' => second_monster.id, 'name' => second_monster.name }
        ]
      })
    end
  end

  describe 'POST create' do
    context 'with valid params' do
      it 'creates a new monster' do
        expect{ create_a_valid_monster }.to change(Monster, :count).by(1)
      end

      it 'returns status as created' do
        create_a_valid_monster
        expect(response.status).to eq 201
      end
    end

    context 'with invalid params' do
      it 'does not create a new monster' do
        expect{ create_a_invalid_monster }.to_not change(Monster, :count)
      end

      it 'returns status as ok' do
        create_a_invalid_monster
        expect(response.status).to eq 200
      end

      it 'returns errors' do
        create_a_invalid_monster
        expect(response.parsed_body).to eq({
          "errors" => ["Name can't be blank"]
        })
      end
    end
  end
end
