require 'rails_helper'

describe Api::CollectedCoinsController do
  let(:user) { create(:user) }

  let(:create_valid_collected_coin) do
    post :create, params: {
      user_id: user.id,
      collected_coin: attributes_for(:collected_coin)
    }
  end

  let(:create_invalid_collected_coin) do
    post :create, params: {
      user_id: user.id,
      collected_coin: {
        value: -50
      }
    }
  end

  describe 'POST create' do
    context 'with valid params' do
      it 'creates a new collected coin' do
        expect{ create_valid_collected_coin }.to change(CollectedCoin, :count).by(1)
      end

      it 'returns status as created' do
        create_valid_collected_coin
        expect(response.status).to eq 201
      end
    end

    context 'with invalid params' do
      it 'does not create a new collected coin' do
        expect{ create_invalid_collected_coin }.to_not change(CollectedCoin, :count)
      end

      it 'returns status as ok' do
        create_invalid_collected_coin
        expect(response.status).to eq 200
      end

      it 'returns errors' do
        create_invalid_collected_coin
        expect(response.parsed_body).to eq({
          "errors" => ["Value must be greater than 0"]
        })
      end
    end
  end
end
