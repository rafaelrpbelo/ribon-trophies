require 'rails_helper'

describe Api::KilledMonstersController do
  let(:user) { create(:user) }
  let(:monster) { create(:monster) }

  let(:create_valid_killed_monster) do
    post :create, params: {
      user_id: user.id,
      killed_monster: {
        monster_id: monster.id
      }
    }
  end

  let(:create_invalid_killed_monster) do
    post :create, params: {
      user_id: user.id,
      killed_monster: {
        monster_id: -500
      }
    }
  end

  describe 'POST create' do
    context 'with valid params' do
      it 'creates a new KilledMonster' do
        expect{ create_valid_killed_monster }.
          to change{ user.killed_monsters.count }.by(1)
      end

      it 'returns status as created' do
        create_valid_killed_monster
        expect(response.status).to eq 201
      end
    end
  end
end
