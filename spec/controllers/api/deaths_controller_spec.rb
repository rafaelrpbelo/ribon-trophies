require 'rails_helper'

describe Api::DeathsController do
  let(:user) { create(:user) }

  let(:create_a_valid_death) do
    post :create, params: {
      user_id: user.id,
      death: attributes_for(:death)
    }
  end

  let(:create_an_invalid_death) do
    post :create, params: {
      user_id: user.id,
      death: { timestamp: 10.minutes.from_now }
    }
  end

  describe 'POST create' do
    context 'with valid params' do
      it 'creates a new Death' do
        expect{ create_a_valid_death }.to change(Death, :count).by(1)
      end

      it 'returns status as created' do
        create_a_valid_death
        expect(response.status).to eq 201
      end
    end

    context 'with invalid params' do
      it 'does not create a new Death' do
        expect{ create_an_invalid_death }.to_not change(Death, :count)
      end

      it 'returns status as ok' do
        create_an_invalid_death
        expect(response.status).to eq 200
      end

      it 'returns errors' do
        create_an_invalid_death
        expect(response.parsed_body).to eq({
          "errors" => ["Timestamp can't be in the future"]
        })
      end
    end
  end
end
