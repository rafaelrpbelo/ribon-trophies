FactoryBot.define do
  factory :monster do
    sequence(:name) {|i| "Monster #{i}" }
  end
end
