FactoryBot.define do
  factory :trophy do
    sequence(:name) {|i| "Trophy #{i}" }
    type { [CollectedCoinTrophy, DeathTrophy, KilledMonsterTrophy].sample.to_s }
    level { 1 }
  end
end
