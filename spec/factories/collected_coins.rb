FactoryBot.define do
  factory :collected_coin do
    user
    value { 10 }
  end
end
