FactoryBot.define do
  factory :death do
    user
    timestamp { rand(1..1_000_000).seconds.ago }
  end
end
