# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: 2.6.4

* Setup

```
# For convencional users
$ cp config/database.yml{.sample,}
$ bundle install
$ yarn install
$ rake db:setup
$ rails s

# For docker users
$ cp Dockerfile{.sample,}
$ cp docker-compose.yml{.sample,}
$ cp .env{.sample,}
$ docker-compose build
$ docker-compose run --rm web bundle install
$ docker-compose run --rm web yarn install
$ docker-compose run --rm web bundle exec rake db:setup
$ docker-compose up web

```

* How to run the test suite

```
$ rspec
```
