Rails.application.routes.draw do
  root 'home#index'

  namespace :api, defaults: { format: :json } do
    resources :users, only: [:index, :show, :create] do
      resources :deaths, only: :create
      resources :collected_coins, only: :create
      resources :killed_monsters, only: :create
    end

    resources :monsters, only: [:index, :create]
  end
end
