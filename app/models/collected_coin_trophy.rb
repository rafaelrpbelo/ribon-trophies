class CollectedCoinTrophy < Trophy
  LEVELS = [
    { number: 1, name: 'collected coin lvl 1', target: 1 },
    { number: 2, name: 'collected coin lvl 2', target: 100 },
    { number: 3, name: 'collected coin lvl 3', target: 1_000 },
    { number: 4, name: 'collected coin lvl 4', target: 10_000 },
    { number: 5, name: 'collected coin lvl 5', target: 100_000 }
  ]

  def self.check_award_for(user)
    amount = user.collected_coins_count || 0

    LEVELS.each do |level|
      next if level[:target] > amount

      create_with(name: level[:name]).
        find_or_create_by(level: level[:number], user: user)
    end
  end
end
