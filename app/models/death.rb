class Death < ApplicationRecord
  validates :timestamp, presence: true
  validate :future_timestamp

  belongs_to :user, counter_cache: true

  after_save :check_awards

  private

  def future_timestamp
    return unless timestamp.present?
    errors.add(:timestamp, "can't be in the future") if timestamp > Time.current
  end

  def check_awards
    Trophy.check_awards_for(user, type: self.class)
  end
end
