class Monster < ApplicationRecord
  validates :name, presence: true
end
