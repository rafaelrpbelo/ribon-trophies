class DeathTrophy < Trophy
  LEVELS = [
    { number: 1, name: 'death lvl 1', target: 1 },
    { number: 2, name: 'death lvl 2', target: 10 },
    { number: 3, name: 'death lvl 3', target: 25 },
    { number: 4, name: 'death lvl 4', target: 50 },
    { number: 5, name: 'death lvl 5', target: 100 }
  ]

  def self.check_award_for(user)
    amount = user.deaths_count || 0

    LEVELS.each do |level|
      next if level[:target] > amount

      create_with(name: level[:name]).
        find_or_create_by(level: level[:number], user: user)
    end
  end
end
