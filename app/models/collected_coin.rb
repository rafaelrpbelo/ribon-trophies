class CollectedCoin < ApplicationRecord
  belongs_to :user, counter_cache: true

  validates :value, numericality: { greater_than: 0 }

  after_save :check_awards

  private

  def check_awards
    Trophy.check_awards_for(user, type: self.class)
  end
end
