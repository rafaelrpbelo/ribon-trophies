class Trophy < ApplicationRecord
  belongs_to :user, counter_cache: true

  def self.check_awards_for(user, type:)
    case type.to_s
    when 'CollectedCoin'
      CollectedCoinTrophy.check_award_for(user)
    when 'Death'
      DeathTrophy.check_award_for(user)
    when 'KilledMonster'
      KilledMonsterTrophy.check_award_for(user)
    else
      raise ArgumentError, "There's no awards checker for #{type}"
    end
  end
end
