class KilledMonster < ApplicationRecord
  belongs_to :user, counter_cache: :all_killed_monsters_count
  belongs_to :monster

  before_save    :increment_killed_monster_counter
  before_destroy :decrement_killed_monster_counter

  after_save :check_awards

  private

  def check_awards
    Trophy.check_awards_for(user, type: self.class)
  end

  def increment_killed_monster_counter
    user.killed_monsters_count[counter_key] ||= 0
    user.killed_monsters_count[counter_key] += 1
    user.save
  end

  def decrement_killed_monster_counter
    return if user.killed_monsters_count[counter_key].nil? ||
      user.killed_monsters_count[counter_key].zero?

    user.killed_monsters_count[counter_key] -= 1
    user.save
  end

  def counter_key
    monster.id.to_s
  end
end
