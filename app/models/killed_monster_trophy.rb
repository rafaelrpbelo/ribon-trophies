class KilledMonsterTrophy < Trophy
  LEVELS = [
    {
      number: 1,
      name: ->(monster_name) { "#{monster_name} killer lvl 1" },
      target: 1
    },
    {
      number: 2,
      name: ->(monster_name) { "#{monster_name} killer lvl 2" },
      target: 100
    },
    {
      number: 3,
      name: ->(monster_name) { "#{monster_name} killer lvl 3" },
      target: 1_000
    },
    {
      number: 4,
      name: ->(monster_name) { "#{monster_name} killer lvl 4" },
      target: 10_000
    },
    {
      number: 5,
      name: ->(monster_name) { "#{monster_name} killer lvl 5" },
      target: 100_000
    }
  ]

  def self.check_award_for(user)
    counter = user.killed_monsters_count || {}

    counter.each do |monster_id, amount|
      LEVELS.each do |level|
        next if level[:target] > amount

        monster = Monster.find(monster_id)

        find_or_create_by(
          name: level[:name].call(monster.name),
          level: level[:number],
          user: user)
      end
    end
  end
end
