json.result do
  json.call(@user, :id, :name, :collected_coins_count, :deaths_count,
  :trophies_count, :all_killed_monsters_count, :killed_monsters_count,
  :created_at, :updated_at)

  json.trophies @user.trophies, :id, :name, :level
end
