class Api::UsersController < Api::BaseController
  def index
    @users = User.all.order(:created_at)
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    if @user.save
      head :created
    else
      render json: { errors: @user.errors.full_messages }
    end
  end

  private

  def user_params
    params.require(:user).permit(:name)
  end
end
