class Api::CollectedCoinsController < Api::BaseController
  def create
    @user = User.find(params[:user_id])
    @collected_coin = @user.collected_coins.build(collected_coin_params)

    if @collected_coin.save
      head :created
    else
      render json: { errors: @collected_coin.errors.full_messages }
    end
  end

  private

  def collected_coin_params
    params.require(:collected_coin).permit(:value)
  end
end
