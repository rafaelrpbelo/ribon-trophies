class Api::DeathsController < Api::BaseController
  def create
    @user = User.find(params[:user_id])
    @death = @user.deaths.build(death_params)

    if @death.save
      head :created
    else
      render json: { errors: @death.errors.full_messages }
    end
  end

  private

  def death_params
    params.require(:death).permit(:timestamp)
  end
end
