class Api::KilledMonstersController < Api::BaseController
  def create
    @user = User.find(params[:user_id])
    @monster = Monster.find(killed_monster_params[:monster_id])
    @killed_monster = @user.killed_monsters.build(monster: @monster)

    if @killed_monster.save
      head :created
    else
      render json: { errors: @killed_monster.errors.full_messages }
    end
  end

  private

  def killed_monster_params
    params.require(:killed_monster).permit(:monster_id)
  end
end
