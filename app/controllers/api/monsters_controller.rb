class Api::MonstersController < Api::BaseController
  def index
    @monsters = Monster.all.order(:created_at)
  end

  def create
    @monster = Monster.new(monster_params)

    if @monster.save
      head :created
    else
      render json: { errors: @monster.errors.full_messages }
    end
  end

  private

  def monster_params
    params.require(:monster).permit(:name)
  end
end
